mod mdp;
use tracing::info;

fn just_like_that() {
    info!("hellow work");
}

#[cfg(test)]
mod tests {
    use test_log::test;
    use tracing::info;

    #[test]
    fn check() {
        let _ = env_logger::builder().is_test(true).try_init();
        info!("Checking whether it still works...");
        assert_eq!(2 + 2, 4);
        info!("Looks good!");
    }
}
