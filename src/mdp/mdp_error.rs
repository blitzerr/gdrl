#[derive(Debug, PartialEq, Eq)]
pub enum MdpError {
    ParseErr(String),
    NoSuchState(String),
    NoSuchAction(String),
    InvalidState(String),
}

impl From<serde_json::Error> for MdpError {
    fn from(value: serde_json::Error) -> Self {
        Self::ParseErr(value.to_string())
    }
}
