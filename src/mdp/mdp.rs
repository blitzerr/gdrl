use std::collections::HashMap;

use eq_float::F64;
use serde_json::{self, Value};

use super::mdp_error::MdpError;

pub struct Mdp {
    s: HashMap<String, Value>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Transitions {
    is_terminal_state: bool,
    next_state: String,
    probab: F64,
    reward: F64,
}

impl TryFrom<&serde_json::Value> for Transitions {
    type Error = MdpError;

    fn try_from(value: &serde_json::Value) -> Result<Self, Self::Error> {
        let arr = value.as_array().unwrap();
        Ok(Self {
            probab: F64(arr[0].as_f64().ok_or_else(|| {
                MdpError::ParseErr(format!("Expected probability '{}' as float", arr[0]))
            })?),
            next_state: arr[1]
                .as_str()
                .ok_or_else(|| {
                    MdpError::ParseErr(format!("Expected next state '{}' as string.", arr[1]))
                })?
                .to_string(),
            reward: F64(arr[2].as_f64().ok_or_else(|| {
                MdpError::ParseErr(format!("Expected reward '{}' as int", arr[2]))
            })?),
            is_terminal_state: arr[3].as_bool().ok_or_else(|| {
                MdpError::ParseErr(format!(
                    "Expected bool to identify terminal state, found {}",
                    arr[3]
                ))
            })?,
        })
    }
}

impl Mdp {
    pub fn new(s: Value) -> Result<Self, MdpError> {
        let mdp = Self {
            s: serde_json::from_value(s)?,
        };
        Self::validate(&mdp)?;
        Ok(mdp)
    }

    // 1. states are parsable i.e their values are object.
    // 2. actions are parsable i.e their values are objects.
    // 3. transitions are parsable and the next state in a transition belongs to the known
    //    states in the state-space/
    // 4. Sum of all transition probabilities is 1.
    fn validate(mdp: &Mdp) -> Result<(), MdpError> {
        let mut states = mdp.states().collect::<Vec<_>>();
        states.sort();

        mdp.states().try_for_each(|s| {
            let mut actions = mdp.actions(s)?;
            actions.try_for_each(|a| {
                let mut sum = 0.0;
                mdp.transitions(s, a)?.try_for_each(|t| {
                    let Transitions { next_state, probab, .. } = t?;
                    sum += probab.0;

                    if states.contains(&&next_state) {
                        Ok(())
                    } else {
                        Err(MdpError::NoSuchState(format!("The provided next-state '{next_state}' does not belong to the state-space: {states:?}")))
                    }
                })?;
                if F64(sum.into()) == F64(1.0) {
                    Ok(())
                } else {
                    Err(MdpError::InvalidState(format!("The probabilities do not sum up to 1 but {sum}.")))
                }
            })?;
            Ok::<(), MdpError>(())
        })
    }

    pub fn states(&self) -> impl Iterator<Item = &String> {
        self.s.keys()
    }

    pub fn actions(&self, state: &str) -> Result<impl Iterator<Item = &String>, MdpError> {
        Ok(self
            .s
            .get(state)
            .ok_or_else(|| MdpError::NoSuchState(state.to_owned()))?
            .as_object()
            .ok_or_else(|| {
                MdpError::ParseErr(format!("Value for state {state} is not an object."))
            })?
            .keys())
    }

    pub fn transitions<'a>(
        &'a self,
        state: &'a str,
        action: &'a str,
    ) -> Result<impl Iterator<Item = Result<Transitions, MdpError>> + 'a, MdpError> {
        Ok(self
            .s
            .get(state)
            .ok_or_else(|| MdpError::NoSuchState(state.to_owned()))?
            .as_object()
            .ok_or_else(|| {
                MdpError::ParseErr(format!("Value for state {state} is not an object."))
            })?
            .get(action)
            .ok_or_else(|| MdpError::NoSuchAction(action.to_owned()))?
            .as_array()
            .ok_or_else(|| {
                MdpError::ParseErr(format!("Value for action {action} is not an object."))
            })?
            .iter()
            .map(|x| Transitions::try_from(x)))
    }
}

#[cfg(test)]
mod tests {
    use eq_float::F64;
    use serde_json::json;

    use crate::mdp::{mdp::Transitions, mdp_error::MdpError};

    use super::Mdp;

    #[test]
    fn test_creation() {
        let mdp = Mdp::new(json!(
        {
            "0": {
                "0": [(1.0, "0", 0.0, true)],
                "1": [(1.0, "0", 0.0, true)]
            },
            "state1": {
                "0": [(0.8, "0", 0.0, true), (0.2, "st2", 1.0, true)],
                "1": [(0.8, "st2", 1.0, true), (0.2, "0", 0.0, true)]
            },
            "st2": {
                "0": [(1.0, "st2", 0.0, true)],
                "1": [(1.0, "0", 0.0, true)]
            }
        }
        ))
        .unwrap();

        let mut states = mdp.states().collect::<Vec<_>>();
        states.sort();
        assert_eq!(states, vec!["0", "st2", "state1",]);

        let mut actions = mdp.actions("state1").unwrap().collect::<Vec<_>>();
        actions.sort();
        assert_eq!(actions, vec!["0", "1"]);

        let e_actions = mdp.actions("st1").err().unwrap();
        assert_eq!(e_actions, MdpError::NoSuchState("st1".to_string()));

        let t: Result<Vec<_>, _> = mdp.transitions("state1", "1").unwrap().collect();
        let t = t.unwrap();
        let expected = vec![
            Transitions {
                is_terminal_state: true,
                next_state: "st2".to_string(),
                probab: F64(0.8),
                reward: F64(1.0),
            },
            Transitions {
                is_terminal_state: true,
                next_state: "0".to_string(),
                probab: F64(0.2),
                reward: F64(0.0),
            },
        ];
        assert_eq!(t, expected);
    }

    #[test]
    fn test_invalid_data_ty() {
        let mdp = Mdp::new(json!(
        {
            "0": {
                "0": [(1.0, "0", 0.0, true)],
                "1": [(1.0, "0", 0.0, true)]
            },
            "state1": {
                "0": [(0.8, "0", 0.0, true), (0.2, "st2", 1.0, true)],
                "1": [(0.8, "st2", 1.0, true), (0.2, "0", 0.0, true)]
            },
            "st2": {
                "0": [(1.0, 2, 0.0, true)],
                "1": [(1.0, 2, 0.0, true)]
            }
        }
        ))
        .err()
        .unwrap();
        assert_eq!(
            mdp,
            MdpError::ParseErr("Expected next state '2' as string.".to_string())
        );
    }

    #[test]
    fn test_next_state_does_not_belong_to_state_space() {
        let mdp = Mdp::new(json!(
        {
            "0": {
                "0": [(1.0, "0", 0.0, true)],
                "1": [(1.0, "0", 0.0, true)]
            },
            "state1": {
                "0": [(0.8, "0", 0.0, true), (0.2, "st2", 1.0, true)],
                "1": [(0.8, "st2", 1.0, true), (0.2, "0", 0.0, true)]
            },
            "st2": {
                "0": [(1.0, "2", 0.0, true)],
                "1": [(1.0, "st2", 0.0, true)]
            }
        }
        ))
        .err()
        .unwrap();
        assert_eq!(
            mdp,
            MdpError::NoSuchState("The provided next-state '2' does not belong to the state-space: [\"0\", \"st2\", \"state1\"]".to_string())
        );
    }

    #[test]
    fn test_sum_of_transitions_not_1() {
        let mdp = Mdp::new(json!(
        {
            "0": {
                "0": [(1.0, "0", 0.0, true)],
                "1": [(1.0, "0", 0.0, true)]
            },
            "state1": {
                "0": [(0.8, "0", 0.0, true), (0.1, "st2", 1.0, true)],
                "1": [(0.8, "st2", 1.0, true), (0.2, "0", 0.0, true)]
            },
            "st2": {
                "0": [(1.0, "state1", 0.0, true)],
                "1": [(1.0, "st2", 0.0, true)]
            }
        }
        ))
        .err()
        .unwrap();
        assert_eq!(
            mdp,
            MdpError::InvalidState("The probabilities do not sum up to 1 but 0.9.".to_string())
        );
    }
}
